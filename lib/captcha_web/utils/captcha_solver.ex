defmodule CaptchaSolver do
    @moduledoc """
    Solve the Captcha.
    """
    
    @doc """
    Transforms the content string in the correctly formatted list of chars.
  
    Since the captcha is circular, it adds the first element of the string to the end of it,
    so it can compare the last element with the first cleanly.
    """
    def get_formatted_content_as_list(content) do
      formatted_content = content <> String.slice(content, 0..0)
      String.graphemes(formatted_content)
    end
    
    @doc """
    Sum duplicated sequential elements.

    It's easier to compare one element with the previous one, than it is to compare with the next one.
    Given that, this approach is done by comparing an element with the previous element for equality,
    and performing the sum if they are equal. The principle and results are the same on both approaches.
    """
    def sum_sequential_duplicates(content_as_list) do
      Enum.reduce(content_as_list, %{previous: nil, sum: 0}, fn(current, acc) ->
        new_accumulated = if current === acc.previous do
          acc.sum + String.to_integer(current)
        else
          acc.sum
        end
        
        %{previous: current, sum: new_accumulated}
      end)
    end
  
    
    @doc """
    Solve the captcha.
    """
    def solve(content) do
      content_as_list = get_formatted_content_as_list(content)
      result = sum_sequential_duplicates(content_as_list)
      result.sum
    end
  end
