defmodule CaptchaWeb.PageController do
  use CaptchaWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
