defmodule CaptchaWeb.CaptchaChannel do
  use CaptchaWeb, :channel

  def join(channel_name, _params, socket) do
    {:ok, %{channel: channel_name}, socket}
  end

  def handle_in("captcha:add", %{"message" => content}, socket) do
    # Get the solved captcha, or `nil` if invalid.
    solved = try do
      content_as_integer = String.to_integer(content)  # Validate if the content can be converted into an integer (valid captcha)
      if content_as_integer < 0 do
        # Must be 0 or positive...
        raise ArgumentError
      end
      CaptchaSolver.solve(content)
    rescue
      ArgumentError -> nil
    end
    
    # Broadcast to everyone, so they know that someone is using this AWESOME captcha solver!
    broadcast!(socket, "captcha:yipee", %{payload: "Someone just used this!"})

    # Actual solution goes only to the request origin.
    {:reply, {:ok, %{original: content, solved: solved}}, socket}
  end
end
