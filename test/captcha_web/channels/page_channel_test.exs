defmodule Captcha.CaptchaChannelTest do
  use CaptchaWeb.ChannelCase

  setup do
    {:ok, _, socket} =
      socket("/socket", %{})
      |> subscribe_and_join(CaptchaWeb.CaptchaChannel, "captcha:solver")

    {:ok, socket: socket}
  end

  test "add valid captchas", %{socket: socket} do
    ref1 = push socket, "captcha:add", %{"message" => "1122"}
    ref2 = push socket, "captcha:add", %{"message" => "1111"}
    ref3 = push socket, "captcha:add", %{"message" => "1234"}
    ref4 = push socket, "captcha:add", %{"message" => "91212129"}
    ref5 = push socket, "captcha:add", %{"message" => "0"}
    ref6 = push socket, "captcha:add", %{"message" => "1"}
    ref7 = push socket, "captcha:add", %{"message" => "1233"}

    assert_reply ref1, :ok, %{original: "1122", solved: 3}
    assert_reply ref2, :ok, %{original: "1111", solved: 4}
    assert_reply ref3, :ok, %{original: "1234", solved: 0}
    assert_reply ref4, :ok, %{original: "91212129", solved: 9}
    assert_reply ref5, :ok, %{original: "0", solved: 0}
    assert_reply ref6, :ok, %{original: "1", solved: 1}
    assert_reply ref7, :ok, %{original: "1233", solved: 3}
  end

  test "add invalid captchas", %{socket: socket} do
    ref1 = push socket, "captcha:add", %{"message" => "1122a"}
    ref2 = push socket, "captcha:add", %{"message" => "abc"}
    ref3 = push socket, "captcha:add", %{"message" => "-1"}

    assert_reply ref1, :ok, %{original: "1122a", solved: nil}
    assert_reply ref2, :ok, %{original: "abc", solved: nil}
    assert_reply ref3, :ok, %{original: "-1", solved: nil}
  end
end
