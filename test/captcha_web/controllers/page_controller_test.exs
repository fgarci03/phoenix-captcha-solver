defmodule CaptchaWeb.PageControllerTest do
  use CaptchaWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Welcome to Captcha!"
  end
end
