import { Socket } from 'phoenix';

let socket = new Socket('/socket');
socket.connect();

let channel = socket.channel('captcha:solver');
channel.join()
  .receive('ok', resp => { console.log('Joined successfully', resp) })
  .receive('error', resp => { console.log('Unable to join', resp) });

channel.on("captcha:yipee", (message) => {
  console.log("Yipee: ", message.payload)
});

export default socket;


/**
 * Validate that the content is an integer.
 */
const isInteger = function(content) {
  return /^\+?\d+$/.test(content);
};

/**
 * Render the captcha solution.
 */
const renderCaptcha = function(captcha) {
  let result = 'Invalid';
  if (isInteger(captcha.solved)) {
    result = captcha.solved;
  }
  const captchaTemplate = `<li class="list-group-item">${captcha.original}: <strong>${result}</strong></li>`;
  document.getElementById('captchas').innerHTML += captchaTemplate;
};

/**
 * Push the captcha to the backend and clean the input.
 */
document.getElementById('new-captcha').addEventListener('submit', (e) => {
  e.preventDefault();
  let captchaInput = e.target.querySelector('#captcha-content');
  channel
    .push('captcha:add', { message: captchaInput.value })
    .receive('ok', captcha => { renderCaptcha(captcha) });

  captchaInput.value = '';
  document.getElementById('submit-captcha').disabled = true;
});

/**
 * Enable/Disable the submit button based on the input validation.
 */
document.getElementById('captcha-content').addEventListener('keyup', (e) => {
  document.getElementById('submit-captcha').disabled = !isInteger(e.target.value);
});
